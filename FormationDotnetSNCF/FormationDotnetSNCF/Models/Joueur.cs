﻿namespace FormationDotnetSNCF.Models
{
    public class Joueur
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        // Autres propriétés pertinentes pour un joueur... Numéro, taille, date de naissance, etc.
    }

}
