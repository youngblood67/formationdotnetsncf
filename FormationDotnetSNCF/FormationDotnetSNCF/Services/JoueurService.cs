﻿using FormationDotnetSNCF.Models;

namespace FormationDotnetSNCF.Services
{
    public class JoueurService
    {
        private List<Joueur> joueurs = new List<Joueur>();

        public IEnumerable<Joueur> GetAllJoueurs()
        {
            AjouterJoueur(new Joueur { Nom = "Messi", Prenom = "Lionel" }); 
            AjouterJoueur(new Joueur { Nom = "Ronaldo", Prenom = "Cristiano" });    
            AjouterJoueur(new Joueur { Nom = "Neymar", Prenom = "Junior" });    
            AjouterJoueur(new Joueur { Nom = "Mbappé", Prenom = "Kylian" });
            AjouterJoueur(new Joueur { Nom = "Griezmann", Prenom = "Antoine" });
            return joueurs;
        }

        public void AjouterJoueur(Joueur joueur)
        {
            joueur.Id = joueurs.Any() ? joueurs.Max(j => j.Id) + 1 : 1;
            joueurs.Add(joueur);
        }

        // Autres opérations liées aux joueurs...
    }

}
